from data.data_loader import load_partition_data, load_batch_level_dataset_main, darpa_split


(
        train_data_num,
        val_data_num,
        test_data_num,
        train_data_global,
        val_data_global,
        test_data_global,
        data_local_num_dict,
        train_data_local_dict,
        val_data_local_dict,
        test_data_local_dict,
    ) = load_partition_data(None, 3, "streamspot") 
